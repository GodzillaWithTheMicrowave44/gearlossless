package com.godzillawiththemicrowave.GearLossless;

import org.bukkit.plugin.java.JavaPlugin;

public class GearLossless extends JavaPlugin {

	@Override
	public void onEnable() {
		getLogger().info("Item Despawn Prevention Plugin has been enabled!");
		// Load configuration
		saveDefaultConfig();

		// Register your event listener here
		getServer().getPluginManager().registerEvents(new ItemDespawnListener(this), this);
	}

	@Override
	public void onDisable() {
		getLogger().info("Item Despawn Prevention Plugin has been disabled!");
	}

}
