package com.godzillawiththemicrowave.GearLossless;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemDespawnEvent;

public class ItemDespawnListener implements Listener {
	private final GearLossless plugin;

	public ItemDespawnListener(GearLossless plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onItemDespawn(ItemDespawnEvent event) {
		FileConfiguration config = plugin.getConfig();

		// Retrieve the despawn time from the configuration file
		int despawnTime = config.getInt("despawntime");

		// Debugging: Log the despawn time retrieved from the configuration file
		plugin.getLogger().info("Despawn time configured: " + despawnTime + " ticks");

		// Check if despawn time has passed
		if (event.getEntity().getTicksLived() >= (despawnTime * 20)) {
			// Debugging: Log the ticks lived by the item
			plugin.getLogger().info("Ticks lived by the item: " + event.getEntity().getTicksLived());

			// Debugging: Log that the despawn event is being cancelled
			plugin.getLogger().info("Despawn event cancelled.");

			// Cancel the despawn event
			event.setCancelled(true);
		}
	}
}
